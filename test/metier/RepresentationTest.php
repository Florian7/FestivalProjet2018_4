<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Representation Test</title>
    </head>
    <body>
        <?php
        use modele\metier\Representation;
        use modele\metier\Lieu;
        use modele\metier\Groupe;
        require_once __DIR__ . '/../../includes/autoload.inc.php';
        echo "<h2>Test unitaire de la classe métier Representation</h2>";
        $unGroupe = new Groupe ('g045','Panama Fuerte Raza', null,null,14,'Panama','N');
        $unLieu = new Lieu ('L5','Salle de concert', '42 rue des perdus','500');
        $objet = new Representation('R01',"11/07/2017","20:30","20:45", $unGroupe, $unLieu);
        var_dump($objet);
        ?>
    </body>
</html>

