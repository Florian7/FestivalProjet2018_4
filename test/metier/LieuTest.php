<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Lieu Test</title>
    </head>
    <body>
        <?php
        use modele\metier\Lieu;
        require_once __DIR__ . '/../../includes/autoload.inc.php';
        echo "<h2>Test unitaire de la classe métier Lieu</h2>";
        $objet = new Lieu("L15","L'auberge des nains","6 rue d'alonville","30");
        var_dump($objet);
        ?>
    </body>
</html>
