<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>RepresentationDAO : test</title>
    </head>

    <body>

        <?php

        use modele\dao\RepresentationDAO;
        use modele\dao\LieuDAO;
        use modele\dao\GroupeDAO;
        use modele\dao\Bdd;
        use modele\metier\Representation;
        use modele\metier\Lieu;
        use modele\metier\groupe;
        use controleur\Session;

require_once __DIR__ . '/../../includes/autoload.inc.php';

        $id = 'R01';
        Session::demarrer();
        Bdd::connecter();

        echo "<h2>1- RepresentationDAO</h2>";

        // Test n°1
        echo "<h3>Test getOneById</h3>";
        try {
            $objet = RepresentationDAO::getOneById($id);
            var_dump($objet);
        } catch (Exception $ex) {
            echo "<h4>*** échec de la requête ***</h4>" . $ex->getMessage();
        }

        // Test n°2
        echo "<h3>2- getAll</h3>";
        try {
            $lesObjets = RepresentationDAO::getAll();
            var_dump($lesObjets);
        } catch (Exception $ex) {
            echo "<h4>*** échec de la requête ***</h4>" . $ex->getMessage();
        }

        // Test n°3
        echo "<h3>3- insert</h3>";
        try {
            $id = 'R99';
            $lieu = LieuDao::getOneById ('L4');
            $groupe = GroupeDao::getOneById('g007');
            $objet = new Representation($id, $groupe, $lieu, '11/07/2017', '12:30', '14:00');
            $ok = RepresentationDAO::insert($objet);
            if ($ok) {
                echo "<h4>ooo réussite de l'insertion ooo</h4>";
                $objetLu = RepresentationDAO::getOneById($id);
                var_dump($objetLu);
            } else {
                echo "<h4>*** échec de l'insertion ***</h4>";
            }
        } catch (Exception $e) {
            echo "<h4>*** échec de la requête ***</h4>" . $e->getMessage();
        }

        // Test n°3-bis
        echo "<h3>3- insert déjà présent</h3>";
        try {
            $id = 'R99';
            $objet = new Representation($id, $groupe,$lieu, '11/07/2017', '12:30', '14:00');
            $ok = RepresentationDAO::insert($objet);
            if ($ok) {
                echo "<h4>*** échec du test : l'insertion ne devrait pas réussir  ***</h4>";
                $objetLu = Bdd::getOneById($id);
                var_dump($objetLu);
            } else {
                echo "<h4>ooo réussite du test : l'insertion a logiquement échoué ooo</h4>";
            }
        } catch (Exception $e) {
            echo "<h4>ooo réussite du test : la requête d'insertion a logiquement échoué ooo</h4>" . $e->getMessage();
        }

        // Test n°4
        echo "<h3>4- update</h3>";
        try {
            $objet->setLieu($lieu);
            $objet->setDebut('16:00');
            $ok = RepresentationDAO::update($id, $objet);
            if ($ok) {
                echo "<h4>ooo réussite de la mise à jour ooo</h4>";
                $objetLu = RepresentationDAO::getOneById($id);
                var_dump($objetLu);
            } else {
                echo "<h4>*** échec de la mise à jour ***</h4>";
            }
        } catch (Exception $e) {
            echo "<h4>*** échec de la requête ***</h4>" . $e->getMessage();
        }

       


        // Test n°6
        echo "<h3>7- isAnExistingId</h3>";
        try {
            $id2 = "R02";
            $ok = RepresentationDAO::isAnExistingId($id2);
            $ok = $ok && !RepresentationDAO::isAnExistingId('AZERTY');
            if ($ok) {
                echo "<h4>ooo test réussi ooo</h4>";
            } else {
                echo "<h4>*** échec du test ***</h4>";
            }
        } catch (Exception $e) {
            echo "<h4>*** échec de la requête ***</h4>" . $e->getMessage();
        }

        // Test n°7
        echo "<h3>7- isAnExistingCreneau</h3>";
        try {
            // id et nom d'un établissement existant
            $objet;
            // en mode modification (1er paramètre = false)
            $ok = RepresentationDAO::isAnExistingCreneau(true,$objet);
            $ok = $ok && !RepresentationDAO::isAnExistingId('AZERTY');
            if ($ok) {
                echo "<h4>ooo créneau disponible ooo</h4>";
            } else {
                echo "<h4>*** créneau indisponible ***</h4>";
            }
        } catch (Exception $e) {
            echo "<h4>*** échec de la requête ***</h4>" . $e->getMessage();
        }
        
         // Test n°8
        echo "<h3>8- isAnExistingGroupRepres</h3>";
        try {
            
            // en mode modification (1er paramètre = false)
            $ok = RepresentationDAO::isAnExistingGroupRepres(true,$objet);
            $ok = $ok && !RepresentationDAO::isAnExistingId('AZERTY');
            if ($ok) {
                echo "<h4>ooo le groupe est disponible sur ce créneau ooo</h4>";
            } else {
                echo "<h4>*** le groupe n'est pas disponible sur ce créneau ***</h4>";
            }
        } catch (Exception $e) {
            echo "<h4>*** échec de la requête ***</h4>" . $e->getMessage();
        }
        
         // Test n°4
        echo "<h3>4- heure Limite</h3>";
        try {
            $objet->setFin('24:30');
            var_dump($objet);
            $ok = RepresentationDAO::heureLimite($objet);
            if ($ok) {
                echo "<h4>ooo L'heure de fin est avant 00h00 ooo</h4>";
            } else {
                echo "<h4>*** L'heure de fin est après 00h00 ***</h4>";
            }
        } catch (Exception $e) {
            echo "<h4>*** échec de la requête ***</h4>" . $e->getMessage();
        }
        
        
         // Test n°5
        echo "<h3>5- delete</h3>";
        try {
            $ok = RepresentationDAO::delete($id);
//            $ok = RepresentationDAO::delete("xxx");
            if ($ok) {
                echo "<h4>ooo réussite de la suppression ooo</h4>";
            } else {
                echo "<h4>*** échec de la suppression ***</h4>";
            }
        } catch (Exception $e) {
            echo "<h4>*** échec de la requête ***</h4>" . $e->getMessage();
        }
        
        

        Bdd::deconnecter();
        Session::arreter();
        ?>


    </body>
</html>
