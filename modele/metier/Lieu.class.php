<?php
namespace modele\metier;

/**
 * Description of Lieu
 *
 * @author nicolas Ragazzi
 */
class Lieu {
   /**
     * identifiant du lieu ("lxxx")
     * @var string
     */
    private $id;
    /**
     * nom du lieu
     * @var string
     */
    private $nom;
    /**
     * adresse du lieu
     * @var string 
     */

    private $adresse;
    /**
     * capacite d'accueil du lieu
     * @var integer
     */
    private $capacite;
   
   

    function __construct($id, $nom,$adresse, $capacite) {
        $this->id = $id;
        $this->nom = $nom;
        $this->adresse = $adresse;
        $this->capacite = $capacite;
    }

    function getId() {
        return $this->id;
    }

    function getNom() {
        return $this->nom;
    }

    function getAdresse() {
        return $this->adresse;
    }

    function getCapacite() {
        return $this->capacite;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setNom($nom) {
        $this->nom = $nom;
    }

    function setAdresse($adresse) {
        $this->adresse = $adresse;
    }

    function setCapacite($capacite) {
        $this->capacite = $capacite;
    }




}
