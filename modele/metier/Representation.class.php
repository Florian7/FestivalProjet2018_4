<?php
namespace modele\metier;

/**
 * Description of Representation
 *
 * @author arthur CLEMENCEAU
 */
class Representation {
   /**
     * identifiant de la representation ("Rxx")
     * @var string
     */
    private $id;
    /**
     * nom du groupe
     * @var Groupe
     */
    private $groupe;
    /**
     * lieu
     * @var Lieu 
     */

    private $lieu;
    /**
     * date de l'evenement
     * @var integer
     */
    private $jour;
    /**
     * heure de debut
     * @var integer
     */
    private $debut;
    /**
     * heure de fin 
     * @var integer
     */
    private $fin;
   
    function __construct($id, Groupe $groupe, Lieu $lieu, $jour, $debut, $fin) {
        $this->id = $id;
        $this->groupe = $groupe;
        $this->lieu = $lieu;
        $this->jour = $jour;
        $this->debut = $debut;
        $this->fin = $fin;
    }

    function getId() {
        return $this->id;
    }

    function getGroupe(): Groupe {
        return $this->groupe;
    }

    function getLieu(): Lieu {
        return $this->lieu;
    }

    function getJour() {
        return $this->jour;
    }

    function getDebut() {
        return $this->debut;
    }

    function getFin() {
        return $this->fin;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setGroupe(Groupe $groupe) {
        $this->groupe = $groupe;
    }

    function setLieu(Lieu $lieu) {
        $this->lieu = $lieu;
    }

    function setJour($jour) {
        $this->jour = $jour;
    }

    function setDebut($debut) {
        $this->debut = $debut;
    }

    function setFin($fin) {
        $this->fin = $fin;
    }





}


