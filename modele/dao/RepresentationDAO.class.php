<?php
 
namespace modele\dao;

use modele\metier\Representation;
use modele\dao\AttributionDAO;
use PDOStatement;
use PDO;

/**
 * Description of RepresentationDAO
 * Classe métier :  Representation
 * @author btssio
 */
class RepresentationDAO {

    /**
     * Instancier un objet de la classe Representation à partir d'un enregistrement de la table REPRESENTATION
     * @param array $enreg
     * @return Representation
     */
    protected static function enregVersMetier(array $enreg) {
        $id = $enreg['ID'];
        $groupe = $enreg['GROUPE'];
        $lieu = $enreg['LIEU'];
        $jour = $enreg['JOUR'];
        $debut = $enreg['DEBUT'];
        $fin = $enreg['FIN'];


        $uneRepresentation = new Representation($id,GroupeDAO::getOneById($groupe), LieuDAO::getOneById($lieu), $jour, $debut, $fin );

        return $uneRepresentation;
    }

    /**
     * Valorise les paramètres d'une requête préparée avec l'état d'un objet Representation
     * @param type $objetMetier une Representation
     * @param type $stmt requête préparée
     */
    protected static function metierVersEnreg(Representation $objetMetier, PDOStatement $stmt) {
        // On utilise bindValue plutôt que bindParam pour éviter des variables intermédiaires
        // Note : bindParam requiert une référence de variable en paramètre n°2 ; 
        // avec bindParam, la valeur affectée à la requête évoluerait avec celle de la variable sans
        // qu'il soit besoin de refaire un appel explicite à bindParam
        $stmt->bindValue(':id', $objetMetier->getId());
        $stmt->bindValue(':groupe', $objetMetier->getGroupe()->getId());
        $stmt->bindValue(':lieu', $objetMetier->getLieu()->getId());
        $stmt->bindValue(':jour', $objetMetier->getJour());
        $stmt->bindValue(':debut', $objetMetier->getDebut());
        $stmt->bindValue(':fin', $objetMetier->getFin());
    }

    /**
     * Retourne la liste de toutes les Representations
     * @return array tableau d'objets de type Representation
     */
    public static function getAll() {
        $lesObjets = array();
        $requete = "SELECT * FROM Representation";
        $stmt = Bdd::getPdo()->prepare($requete);
        $ok = $stmt->execute();
        if ($ok) {
            // Pour chaque enregisterement
            while ($enreg = $stmt->fetch(PDO::FETCH_ASSOC)) {
                // instancier un Etablissement et l'ajouter au tableau
                $lesObjets[] = self::enregVersMetier($enreg);
            }
        }
        return $lesObjets;
    }

    /**
     * Recherche d'une representation selon la valeur de son identifiant
     * @param string $id
     * @return Representation la representation trouvé ; null sinon
     */
    public static function getOneById($id) {
        $objetConstruit = null;
        $requete = "SELECT * FROM Representation WHERE ID = :id";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':id', $id);
        $ok = $stmt->execute();
        // attention, $ok = true pour un select ne retournant aucune ligne
        if ($ok && $stmt->rowCount() > 0) {
            $objetConstruit = self::enregVersMetier($stmt->fetch(PDO::FETCH_ASSOC));
        }
        return $objetConstruit;
    }

    /**
     * Insérer un nouvel enregistrement dans la table à partir de l'état d'un objet métier
     * @param Representation $objet objet métier à insérer
     * @return boolean =FALSE si l'opération échoue
     */
    public static function insert(Representation $objet) {
        $requete = "INSERT INTO Representation VALUES (:id, :groupe, :lieu, :jour, :debut, :fin)";
        $stmt = Bdd::getPdo()->prepare($requete);
        self::metierVersEnreg($objet, $stmt);
        $ok = $stmt->execute();
        return ($ok && $stmt->rowCount() > 0);
    }

    /**
     * Mettre à jour enregistrement dans la table à partir de l'état d'un objet métier
     * @param string identifiant de l'enregistrement à mettre à jour
     * @param Representation $objet objet métier à mettre à jour
     * @return boolean =FALSE si l'opérationn échoue
     */
    public static function update($id, Representation $objet) {
        $ok = false;
        $requete = "UPDATE  Representation SET GROUPE=:groupe, LIEU=:lieu,
           JOUR=:jour, DEBUT=:debut, FIN=:fin
           WHERE ID=:id";
        $stmt = Bdd::getPdo()->prepare($requete);
        self::metierVersEnreg($objet, $stmt);
        $stmt->bindParam(':id', $id);
        $ok = $stmt->execute();
        return ($ok && $stmt->rowCount() > 0);
    }

    /**
     * Détruire un enregistrement de la table REPRESENTATION d'après son identifiant
     * @param string identifiant de l'enregistrement à détruire
     * @return boolean =TRUE si l'enregistrement est détruit, =FALSE si l'opération échoue
     */
    public static function delete($id) {
        $ok = false;
        $requete = "DELETE FROM Representation WHERE ID = :id";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':id', $id);
        $ok = $stmt->execute();
        $ok = $ok && ($stmt->rowCount() > 0);
        return $ok;
    }

    /**
     * Permet de vérifier s'il existe ou non une Représentation ayant déjà le même identifiant dans la BD
     * @param string $id identifiant de la représentation à tester
     * @return boolean =true si l'id existe déjà, =false sinon
     */
    public static function isAnExistingId($id) {
        $requete = "SELECT COUNT(*) FROM Representation WHERE ID=:id";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        return $stmt->fetchColumn(0);
    }

//
        /**
         * Permet de vérifier s'il existe ou non une représenation au même moment dans la BD
         * En mode modification, l'enregistrement en cours de modification est bien entendu exclu du test
         * @param boolean $estModeCreation =true si le test est fait en mode création, =false en mode modification
         * @param string $id identifiant de la représentation à tester
         * @param string $groupe groupe de la représentation à tester
         * @param string $lieu lieu de la représentation à tester
         * @param string $jour jour de la représentation à tester
         * @param string $debut heure de début de la représentation à tester
         * @return boolean =true si le nom existe déjà, =false sinon
         */
        public static function isAnExistingCreneau($estModeCreation, Representation $uneRepresentation) {
            
            $lieuRepres = $uneRepresentation->getLieu();
            $dateRepres = $uneRepresentation->getJour();
            $heureDebRepres = $uneRepresentation->getDebut();
            $idRepresentation = $uneRepresentation->getId();
            $idLieu=$lieuRepres->getId();
            if ($estModeCreation) {
                $requete = "SELECT COUNT(*) FROM Representation WHERE JOUR=:dateRepres AND DEBUT=:heureDebRepres AND LIEU=:lieuRepres";
                $stmt = Bdd::getPdo()->prepare($requete);
                $stmt->bindParam(':dateRepres', $dateRepres);
                $stmt->bindParam(':heureDebRepres', $heureDebRepres);
                $stmt->bindParam(':lieuRepres', $idLieu);
                $stmt->execute();
            } else {
                $requete = "SELECT COUNT(*) FROM Representation WHERE JOUR=:dateRepres AND DEBUT=:heureDebRepres AND LIEU=:lieuRepres AND ID<>:idRepresentation";
                $stmt = Bdd::getPdo()->prepare($requete);
                $stmt->bindParam(':idRepresentation', $idRepresentation);
                $stmt->bindParam(':heureDebRepres', $heureDebRepres);
                $stmt->bindParam(':dateRepres', $dateRepres);
                $stmt->bindParam(':lieuRepres', $idLieu);
                $stmt->execute();
            }
            return $stmt->fetchColumn(0);
        }
        
        public static function isAnExistingGroupRepres($estModeCreation, Representation $uneRepresentation) {
            $id = $uneRepresentation->getId();
            $dateRepres = $uneRepresentation->getJour();
            $heureDebRepres = $uneRepresentation->getDebut();
            //$heureFin = $uneRepresentation->getFin();
            $groupe = $uneRepresentation->getGroupe();
            $idGroupe = $groupe->getId();
            // S'il s'agit d'une création, on vérifie juste la non existence du nom sinon
            // on vérifie la non existence d'un autre groupe (id!='$id') portant 
            // le même nom
            if ($estModeCreation) {
                $requete = "SELECT COUNT(*) FROM Representation WHERE JOUR=:dateRepres AND DEBUT=:heureDebRepres AND GROUPE=:idGroupe "; 
                $stmt = Bdd::getPdo()->prepare($requete);
                $stmt->bindParam(':dateRepres', $dateRepres);
                $stmt->bindParam(':heureDebRepres', $heureDebRepres);
                //$stmt->bindParam(':heureFin', $heureFin);
                $stmt->bindParam(':idGroupe', $idGroupe);
                $stmt->execute();
            } else {
                $requete = "SELECT COUNT(*) FROM Representation WHERE JOUR=:dateRepres AND DEBUT=:heureDebRepres AND GROUPE=:idGroupe AND ID<>:id"; 
                $stmt = Bdd::getPdo()->prepare($requete);
                $stmt->bindParam(':id', $id);
                $stmt->bindParam(':dateRepres', $dateRepres);
                $stmt->bindParam(':heureDebRepres', $heureDebRepres);
                //$stmt->bindParam(':heureFin', $heureFin);
                $stmt->bindParam(':idGroupe', $idGroupe);
                $stmt->execute();
            }
            return $stmt->fetchColumn(0);
        }
        public static function heureLimite (Representation $uneRepresentation){
            $heureFin = $uneRepresentation->getFin();
            $ok = false;
            if ($heureFin<='24:00'){
                $ok = true;
            }
            return $ok;
        }
        
        public static function getRepresentationByDate ($date){
            $requete="SELECT * FROM Representation WHERE JOUR = :date ORDER BY LIEU ";
            $stmt=Bdd::getPdo()->prepare($requete);
            $stmt->bindParam(':date', $date);
            $ok=$stmt->execute();
            if ($ok){
                //pour chaque enregistrement
                while($enreg=$stmt->fetch(PDO::FETCH_ASSOC)){
                    //instancer une representation et l'ajouter au tableau
                    $lesObjets[]=self::enregVersMetier($enreg);
                }
            }
            return $lesObjets;
            
        }
    
    
    
    
    
    }
    