<?php
/**
 * Description of CtrlRepresentation
 *
 * @author nragazzi
 */

namespace controleur;

use modele\dao\RepresentationDAO;
use modele\dao\Bdd;
use vue\representation\VueRepresentation;





class CtrlRepresentation extends ControleurGenerique {
    /** controleur= representation & action= defaut
     * Afficher la liste des offres d'hébergement      */
    public function defaut() {
        $this->consulter();
    }

    /** controleur= representation & action= consulter
     * Afficher la liste des Représenttions      */
    function consulter() {
        $laVue = new VueRepresentation();
        $this->vue = $laVue;
        // La vue a besoin de la liste des Représentations
        Bdd::connecter();
        $laVue->setLesRepresentations(RepresentationDAO::getAll());
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - Representation");
        $this->vue->setVersion($this->version);
        $this->vue->afficher();
    }

//    /** controleur= Representation & action= modifier & id = identifiant de la représentation
//     * Modifier les offres d'hébergement proposées par un établissement  */
//    function modifier() {
//        $laVue = new VueSaisieOffres
//        $this->vue = $laVue;
//        // Lecture de l'id de l'établissement
//        $idEtab = $_GET['id'];
//        // La vue a besoin :
//        //  - de l'établissement concerné, 
//        //  - des l'ensemble des types de chambres
//        //  - des offres par type de chambre pour cet établissement
//        //  - des attributions déjà effectuées par type de chambre pour cet établissement        
//        Bdd::connecter();
//        $laVue->setUnEtablissement(EtablissementDAO::getOneById($idEtab));
//        $laVue->setLesTypesChambres(TypeChambreDAO::getAll());
//        $laVue->setTabNbChambresAffiches($this->getTabNbChambresOffertesParTypePourUnEtab($idEtab));
//        $laVue->setTabNbChambresAttribues($this->getTabNbChambresAttribueesParTypePourUnEtab($idEtab));
//        parent::controlerVueAutorisee();
//        $this->vue->setTitre("Festival - offres");
//        $this->vue->setVersion($this->version);
//        $this->vue->afficher();
//    }


}
